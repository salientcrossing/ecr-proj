
provider "aws" {
    region = "us-east-1"
}

resource "aws_ecr_repository" "docker_tar" {
  name                 = "docker-tar"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

output "repository_url" {
  description = "The URI of the repository."
  value = aws_ecr_repository.docker_tar.repository_url
}