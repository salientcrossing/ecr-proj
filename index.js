
const express = require('express');
const dotenv = require('dotenv');

dotenv.config();

const app = express();
const port = process.env.PORT || 8070;

app.get('/', (req, res) => {
  res.send('He shall have dominion also from sea to sea, And from the River to the ends of the earth.');
});

app.listen(port, () => {
  console.log(`[server]: Server is running at http://localhost:8070`);
});