
FROM node:14

# Set the working directory for the application
WORKDIR /app

# Copy the package.json and package-lock.json files to the working directory
COPY package*.json ./

# Install the dependencies for the application
RUN npm install

# Copy the application file to the working directory
COPY index.js .

# Expose the port on which the application will listen
EXPOSE 8070

# Start the application when the Docker container is launched
CMD ["node", "index.js"]